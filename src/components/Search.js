import React from 'react';
import styled from 'styled-components'

class Search extends React.Component {
    state = {};

    //поле поиска
    render() {
        return (
            <SearchContent>
                <SearchContentInput onChange={(evt) => this.props.searchContent(evt)} type="text" placeholder="Search" value={this.props.searchString}/>
            </SearchContent>
        )
    };
}


export default Search;

const SearchContent = styled.div`
    margin: 10px 0;
`

const SearchContentInput = styled.input`
    
`