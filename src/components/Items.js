import React from 'react';
import Item from './Item';
import Search from './Search';
import styled from 'styled-components'

export default class Items extends React.Component {
    state = {
        tasks:                 [
            {title: 'Молоко', description: 'Необходимо купить молоко', completed: false},
            {title: 'Собака', description: 'Выгулять собаку', completed: false},
            {title: 'Угорь', description: 'Съесть угря', completed: false}
        ],
        searchString:          '',
        filteredTasks:         [],
        checkedCompletedTasks: '',
        newTitle:              '',
        newDescription:        '',
        gg: '',
    };

    //рендеринг тасков
    eachTask = (item, i) => {
        return (
            <Item key={i} index={i} updateTask={this.updateTask} deleteBlock={this.deleteBlock} completedTask={this.completedTask}>
                {item}
            </Item>);
    };

    //строка поиска тасков
    searchContent = (event) => {
        this.setState({searchString: event.target.value.toLowerCase()});
    };

    //сам поиск тасков
    searchTask = (tasks, searchString) => {
        if (searchString.length === 0) {
            return tasks;
        }

        const filterVariable = task => task.title.toLowerCase().includes(searchString) || task.description.toLowerCase().includes(searchString);

        return tasks.filter(task => {
            return filterVariable(task);
        });
    };

    //удаление тасков
    deleteBlock = (i) => {
        let arrTasks = this.state.tasks;
        arrTasks.splice(i, 1);
        this.setState({tasks: arrTasks, filteredTasks: arrTasks});
    };

    //изменение тасков
    updateTask = (title, description, completed, i) => {
        let arrTasks = this.state.tasks;
        arrTasks[i]  = {title, description, completed};
        this.setState({tasks: arrTasks, filteredTasks: arrTasks});
    };

    //добавление новых тасков
    addTask = (title, description) => {
        let arrTasks = this.state.tasks;
        arrTasks.push({title, description, completed: false});
        this.setState({tasks: arrTasks, filteredTasks: arrTasks, newTitle: '', newDescription: ''});
    };

    //отметка таска как "Выполненного"
    completedTask = (children) => {

        let arrTasks = this.state.tasks;

        let taskIndex = null;
        arrTasks.map((task, index) => {
            if (task === children) {
                taskIndex = index;
            }
        });

        arrTasks[taskIndex] = {...arrTasks[taskIndex], completed: !arrTasks[taskIndex].completed};

        this.setState({tasks: arrTasks, filteredTasks: arrTasks});
    };

    //фильтрация по выполненным таскам
    filterCompleteTask = () => {

        this.setState({
            checkedCompletedTasks: true
        });

        const newFilteredTasks = this.state.tasks.filter(item => {
            return item.completed;
        });

        this.setState({
            filteredTasks: newFilteredTasks
        });
    };

    //фильтрация по невыполненным таскам
    filterUnCompleteTask = () => {
        this.setState({
            checkedCompletedTasks: false
        });

        const newFilteredTasks = this.state.tasks.filter(item => {
            return !item.completed;
        });

        this.setState({
            filteredTasks: newFilteredTasks
        });
    };

    //кнопка сброса
    filterCompleteTaskReset = () => {
        let arrTasks = this.state.tasks;

        arrTasks.forEach((task) => task.completed = '');

        this.setState({
            checkedCompletedTasks: '',
            searchString: '',
            newTitle: '',
            newDescription: '',
        });
    };

    //основной рендер страницы
    render() {
        const visibleItems = (this.state.checkedCompletedTasks !== '') ? this.searchTask(this.state.filteredTasks, this.state.searchString) : this.searchTask(this.state.tasks, this.state.searchString);

        return (
            <WrapperItems>
                <WrapperItemsTitle>List element</WrapperItemsTitle>

                <Search searchContent={evt => this.searchContent(evt)} searchString={this.state.searchString}/>

                <WrapperItemAddTitle>New list element</WrapperItemAddTitle>

                <ItemAddInput placeholder="Name" onChange={(event) => this.setState({newTitle: event.target.value})} value={this.state.newTitle}/>
                <ItemAddInput placeholder="Description" onChange={(event) => this.setState({newDescription: event.target.value})} value={this.state.newDescription}/>
                <ItemButtonAddTask onClick={this.addTask.bind(null, this.state.newTitle, this.state.newDescription)}
                                   disabled={((!this.state.newTitle) || (!this.state.newDescription))}>Добавить задание</ItemButtonAddTask>

                <ItemsFilterWrapper>
                    <ItemsFilterTitle>Tasks</ItemsFilterTitle>
                    <ItemsButtonCompleted onClick={this.filterCompleteTask}>Completed</ItemsButtonCompleted>
                    <ItemsButtonUnCompleted onClick={this.filterUnCompleteTask}>Unfulfilled</ItemsButtonUnCompleted>
                    <ItemsButtonCompletedReset onClick={this.filterCompleteTaskReset}>Reset</ItemsButtonCompletedReset>
                </ItemsFilterWrapper>
                {
                    visibleItems.map(this.eachTask)
                }
            </WrapperItems>
        );
    };
}


const WrapperItems = styled.div`
    text-align: center;
    width: 50%;
    margin: 0 auto;
`

const WrapperItemsTitle = styled.h1`

`

const WrapperItemAddTitle = styled.h2`

`

const ItemsFilterWrapper = styled.div`

`

const ItemsFilterTitle = styled.h3`

`

const ItemsButtonCompleted = styled.button`
    background-color: lightgreen;
    outline: none;
    margin: 10px 0;
    padding: 5px 10px;
`

const ItemsButtonUnCompleted = styled.button`
    background-color: brown;
    outline: none;
    margin: 10px 0 10px 10px;
    padding: 5px 10px;
`

const ItemsButtonCompletedReset = styled.button`
    background-color: yellow;
    outline: none;
    margin: 10px 0 10px 10px;
    padding: 5px 10px;
`

const ItemButtonAddTask = styled.button`
    background-color: aquamarine;
    outline: none;
    margin: 10px 0 10px 10px;
    padding: 5px 10px;
    pointer-events: ${(props) => props.disabled ? 'none' : 'auto'};
    cursor: ${(props) => props.disabled ? 'none' : 'pointer'};
    opacity: ${(props) => props.disabled ? '0.5' : '1'};
    box-sizing: border-box;
    border: 2px solid ${(props) => props.disabled ? 'red' : 'yellow'};
    border-radius: ${(props) => props.disabled ? '2px' : '5px'};
    -moz-transition: all 0.4s ease;
    -o-transition: all 0.4s ease;
    transition: all 0.4s ease;
    
    &:hover {
        color: white;    
    }
`

const ItemAddInput = styled.input`
    margin-right: 10px;
`
