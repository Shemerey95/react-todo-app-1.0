import React from 'react';
import styled from 'styled-components'

export default class Item extends React.Component {
    state = {
        edit: false,
        newTitle: this.props.children.title,
        newDescription: this.props.children.description,
    };

    //открытие формы редактирования таска
    edit = () => {
        this.setState({edit: true});
    };

    //удаление таска
    remove = () => {
        this.props.deleteBlock(this.props.index);
    };

    //сохранение измененного таска
    save = () => {
        this.props.updateTask(this.state.newTitle, this.state.newDescription, this.props.children.completed, this.props.index);
        this.setState({edit: false});
    };

    //отметка выполненных тасков
    done = () => {
        this.props.completedTask(this.props.children);
    };

    //таск для рендеринга
    rendNorm = () => {
        return (
            <ItemsItemBox completed={this.props.children.completed}>
                <ItemsItemLayout onClick={this.done} />
                <ItemsItemTittle onClick={this.done}>{this.props.children.title}</ItemsItemTittle>
                <ItemsItemDescription onClick={this.done}>{this.props.children.description}</ItemsItemDescription>
                <ItemsItemButtonEdit onClick={this.edit}>Редактировать</ItemsItemButtonEdit>
                <ItemsItemButtonDelete onClick={this.remove}>Удалить</ItemsItemButtonDelete>
            </ItemsItemBox>
        );
    };

    //форма редактирования таска
    rendEdit = () => {
        return (
            <ItemsItemEdit>
                <ItemsItemEditTextArea onChange={(event) => this.setState({newTitle: event.target.value})} defaultValue={this.props.children.title} />
                <ItemsItemEditTextArea onChange={(event) => this.setState({newDescription: event.target.value})} defaultValue={this.props.children.description} />
                <ItemsItemEditButton onClick={this.save}>Сохранить</ItemsItemEditButton>
            </ItemsItemEdit>
        );
    };

    //рендер изменения тасков
    render() {
        if (this.state.edit) {
            return this.rendEdit();
        } else {
            return this.rendNorm();
        }
    }
}


const ItemsItemBox = styled.div`
    border: 1px solid grey;
    margin: 5px 0;
    padding-bottom: 5px;
    background: ${(props) => props.completed ? 'lightgreen' : '#ffffff'};
    position: relative;
    z-index: 3;
`

const ItemsItemButtonEdit = styled.button`
    background-color: aquamarine;
    margin: 0 5px;
    width: fit-content;
`

const ItemsItemButtonDelete = styled.button`
    background-color: aquamarine;
    margin: 0 5px;
    width: fit-content;
`

const ItemsItemTittle = styled.h3`
    width: fit-content;
    margin: 10px auto;
`

const ItemsItemDescription = styled.p`
    width: fit-content;
    margin: 10px auto;
`

const ItemsItemLayout = styled.div`
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: -1;
`

const ItemsItemEdit = styled.div`
    border: 1px solid grey;
    padding-bottom: 5px;
    margin-top: 20px;
    padding: 20px 0;
`

const ItemsItemEditButton = styled.button`
    display: block;
    margin: 10px auto 0;
    background-color: aquamarine;
`

const ItemsItemEditTextArea = styled.textarea`
    &:first-child {
        margin-right: 10px;
    }
`
