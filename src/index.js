import React from 'react';
import ReactDOM from 'react-dom';
import Items from './components/Items';

const properties = document.getElementById("root");

ReactDOM.render(
    <Items/>,
    properties
);